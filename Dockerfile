FROM node:8.11

ENV CAMUNDA_URL = 'http://localhost:8080/engine-rest'

WORKDIR /app


COPY package.json /app
RUN npm install
COPY . /app

CMD node worker.js
